/** @jsxImportSource react */
import { qwikify$ } from '@builder.io/qwik-react';
import type { ActionMeta, MultiValue } from 'react-select';
import Select from 'react-select';
import { useId, useState } from 'react';
import { JsonForms } from '@jsonforms/react';
import {
  materialRenderers,
  materialCells,
} from '@jsonforms/material-renderers';

function MySelect({
  placeholder,
  options,
  onSelected,
  itemType,
}: {
  placeholder: string;
  options: any[];
  onSelected: (newVal: MultiValue<never>, actMeta: ActionMeta<never>) => any;
  itemType: 'jurisdiction' | 'game' | 'application' | 'report';
}) {
  let newOptions
  
  if (itemType == 'jurisdiction') {
    newOptions = options.map((value) => {
      return({value: value, label: value}) 
    })
  }

  if (itemType == 'game') {
    if (options[0]['gaming_activity_defs']) {
      const tempArr: any[] = []
      const tempObj: any = {}
      for (const element of options[0]['gaming_activity_defs']) {
        if (!Object.prototype.hasOwnProperty.call(tempObj, element.jurisdiction)) {
          // Key doesn't exist for this jurisdiction
          tempObj[element.jurisdiction] = [element]
        }
        else {
          // Key DOES exist, so lets add the object to the key.
          tempObj[element.jurisdiction].push(element)
        }
      }
      //console.log('tempObj\n', tempObj)

      for (const key in tempObj) {
        //console.log(key, tempObj[key]);
        const optionsArray = tempObj[key].map((element) => {
          return (
            {
              label: element.object.activity_description,
              value: element
            }
          )
        })
        tempArr.push({
          label: key,
          options: optionsArray
        })
      }
      //console.log('tempArr\n', tempArr)
      newOptions = tempArr
    }
  }

  if (itemType == 'application') {
    if(options){
      newOptions = options.map((value) => {
        return({value: value, label: value.object.form_rev}) 
      })
    }
  }

  if (itemType == 'report') {
    newOptions = options.map((value) => {
      return({value: value, label: `...${(value.object_id as string).slice(55)}`}) 
    })
  }

  return (
    <Select
      isMulti={itemType == 'jurisdiction'}
      options={newOptions}
      onChange={(newVal, actMeta) => onSelected(newVal, actMeta)}
      placeholder={placeholder}
      instanceId={useId()}
      styles={{
        menu: (base) => ({
            ...base,
            zIndex: '50'
        }),
      }}
    />
  );
}

function MyJsonForm({
  schema,
  uischema,
  initialData
}:{
  schema: any
  uischema: any
  initialData: any
}){
  const [data, setData] = useState(initialData);
  return (
    <div className='App'>
      <JsonForms
        schema={schema}
        uischema={uischema}
        data={data}
        renderers={materialRenderers}
        cells={materialCells}
        onChange={({ data, errors }) => setData(data)}
      />
    </div>
  );
}

// Convert React component to Qwik component
export const MSelect = qwikify$(MySelect, { eagerness: 'visible' });
export const MJsonForm = qwikify$(MyJsonForm, { eagerness: 'load' });
