import { component$ } from '@builder.io/qwik';

export default component$((props) => {
  return (
    <div class="bg-orange-50 text-sm p-2 mb-4 border-b">

      <div class="grid grid-cols-4 gap-0">
        <div class="col-span-4">
          <span class="font-bold pr-2">Object ID: </span>
          {props.object_id}
        </div>
        <div class="col-span-2 font-italic">
          <span class="font-bold pr-2">Object Type: </span>
          {props.object_type}
        </div>
        <div class="col-span-2">
          <span class="font-bold pr-2">In Block:Trx:Op: </span>
          {props.block_info.in_block} {" : "}
          {props.block_info.trx_in_block} {" : "}
          {props.block_info.op_in_trx}
        </div>
      </div>
    </div>
  );
});
