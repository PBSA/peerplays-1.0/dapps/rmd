import { component$ } from '@builder.io/qwik';

export default component$(() => {

  return (
    <footer>
      <img src={`/Powered by GBC.png`} width="249" height="863"/>
      ⚡️<a href="https://peerplays.com" target="_blank">and Peerplays</a>⚡️
    </footer>
  );
});