import { component$, useSignal, useStore, useTask$, $} from '@builder.io/qwik';
import RmdObjMeta from '../rmd-obj-meta/rmd-obj-meta';
import { MJsonForm } from '../../integrations/react/react';
import hljs from 'highlight.js/lib/core';
import json from 'highlight.js/lib/languages/json';

hljs.registerLanguage('json', json);

interface ItemProps {
  rmd_object: {};
  data_schema?: {};
  ui_schema?: {};
}

const handleCodeDisplay = $(
  function handleCodeDisplay(uglyCode: any) {
    const prettyCode = hljs.highlight(
      JSON.stringify(uglyCode, null, 2),
      { language: 'json' }
    ).value
    return prettyCode
  }
);

export default component$<ItemProps>((props) => {

  const view_is_raw = useSignal(false);
  const store = useStore({
    prettyCode: ""
  });

  useTask$(async () => {
    store.prettyCode = await handleCodeDisplay(props.rmd_object);
  });

  return (
    <>
      <div class="tabs justify-end mt-2">
        <a class={`tab tab-bordered bg-white ${view_is_raw.value ? `` : `tab-active`}`}
           onClick$={
             view_is_raw.value ? $(() => view_is_raw.value = !view_is_raw.value) : undefined
           }>
          Form View
        </a>
        <a class={`tab tab-bordered bg-white ${view_is_raw.value ? `tab-active` : ``}`}
           onClick$={
             view_is_raw.value ? undefined : $(() => view_is_raw.value = !view_is_raw.value)
           }>
          Code View
        </a>
      </div>
      <div class="bg-white p-4">
        <RmdObjMeta
          object_id={(props.rmd_object.object_id as string)}
          object_type={(props.rmd_object.object_type)}
          block_info={(props.rmd_object.block_info)}
        />
        {!view_is_raw.value ? (
          <MJsonForm schema={props.data_schema} uischema={props.ui_schema} initialData={props.rmd_object.object} />
        ):(
          <div class="mockup-code">
            <pre><code dangerouslySetInnerHTML={store.prettyCode}></code>
            </pre>
          </div>
        )}
      </div>
    </>
  );
});
