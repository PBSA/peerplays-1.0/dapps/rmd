import { component$, useStore, $ } from '@builder.io/qwik';
import { MJsonForm } from '~/integrations/react/react';

export default component$(() => {

  const s = useStore({
    dataSchemaInput: {
      "type": "object",
      "properties": {
        "entity_ref": {
          "type": "string"
        },
        "activity_description": {
          "type": "string"
        },
        "date_info": {
          "type": "object",
            "properties": {
              "on_date": {"type": "string"},
              "date_begin": {"type": "string"},
              "date_end": {"type": "string"}
            }
        },
        "venue": {
          "type": "string"
        },
        "rating": {
          "type": "integer"
        }
      },
      "required": [
        "entity_ref",
        "activity_description",
        "venue"
      ]
    },
    uiSchemaInput: {
      "type": "VerticalLayout",
      "elements": [
        {
          "type": "Control",
          "scope": "#/properties/entity_ref",
          "label": "Entity ID"
        },
        {
          "type": "Control",
          "scope": "#/properties/activity_description",
          "options": {
            "multi": true
          },
          "label": "Describe Proposed Gaming Activity"
        },
        {
          "type": "Control",
          "label": "Venue",
          "scope": "#/properties/venue"
        }
      ]
    },
    dataObjectInput: {
      "entity_ref": "bafkreigd7yetbxywttiwwbvuuhxs3peky5grgrsttbbrd36zdancw7easq",
      "activity_description": "50/50 Fundraiser Raffle",
      "date_info": {
        "date_begin": "2023-05-26",
        "date_end": "2023-05-30"
      },
      "venue_info": {
        "event_name": "Remembrance on the River 2023",
        "location": "Weldon, North Carolina"
      },
      "gaming_activity_ref": "bafkreicph3s2wch2wfz7mtapzkzggvamakshakvy4q5ocgcfpfr3a3umzq",
      "jurisdiction": "state_us_north_carolina"
    }
  })

  const handleDataSchema = $((textForUpdate: string) => {
    console.log('textForUpdate', textForUpdate)
    textForUpdate ? s.dataSchemaInput = JSON.parse(textForUpdate) : s.dataSchemaInput = undefined
    console.log('s.dataSchemaInput', s.dataSchemaInput)
  })

  const handleUISchema = $((textForUpdate: string) => {
    console.log('textForUpdate', textForUpdate)
    textForUpdate ? s.uiSchemaInput = JSON.parse(textForUpdate) : s.uiSchemaInput = undefined
    console.log('s.uiSchemaInput', s.uiSchemaInput)
  })

  const handleDataObject = $((textForUpdate: string) => {
    console.log('textForUpdate', textForUpdate)
    textForUpdate ? s.dataObjectInput = JSON.parse(textForUpdate) : s.dataObjectInput = {}
    console.log('s.dataObjectInput', s.dataObjectInput)
  })

  return (
    <div class="mt-4">
      <div class="text-2xl font-bold">Schema Playground</div>

      <div class="flex flex-col w-full gap-3">
        <div class="flex w-full justify-between gap-3">

          <div class="card card-compact card-bordered border-b-4 border-amber-500 bg-stone-50 mt-4 min-h-[400px] w-full">
            <div class="card-body">
              <div class="text-xs font-bold">
                Data Schema
              </div>
              <div id="input-data-schema" 
                   class="w-full h-full p-2 rounded-lg bg-white border-t-2 border-stone-200 whitespace-pre-wrap prose" 
                   contentEditable="true">
                {JSON.stringify(s.dataSchemaInput, null, 2)}
              </div>
              <div class="flex justify-end mt-4">
                <button class="btn btn-sm btn-success mr-2" onClick$={() => handleDataSchema(document.getElementById('input-data-schema')?.innerHTML as string)}>Update Data Schema</button>
              </div>
            </div>
          </div>

          <div class="card card-compact card-bordered border-b-4 border-emerald-500 bg-stone-50 mt-4 min-h-[400px] w-full">
            <div class="card-body">
              <div class="text-xs font-bold">
                UI Schema
              </div>
              <div id="input-ui-schema" 
                   class="w-full h-full p-2 rounded-lg bg-white border-t-2 border-stone-200 whitespace-pre-wrap prose" 
                   contentEditable="true">
                {JSON.stringify(s.uiSchemaInput, null, 2)}
              </div>
              <div class="flex justify-end mt-4">
                <button class="btn btn-sm btn-success mr-2" onClick$={() => handleUISchema(document.getElementById('input-ui-schema')?.innerHTML as string)}>Update UI Schema</button>
              </div>
            </div>
          </div>

          <div class="card card-compact card-bordered border-b-4 border-violet-500 bg-stone-50 mt-4 min-h-[400px] w-full">
            <div class="card-body">
              <div class="text-xs font-bold">
                Input Data Object
              </div>
              <div id="input-data-object" 
                   class="w-full h-full p-2 rounded-lg bg-white border-t-2 border-stone-200 whitespace-pre-wrap prose" 
                   contentEditable="true">
                {JSON.stringify(s.dataObjectInput, null, 2)}
              </div>
              <div class="flex justify-end mt-4">
                <button class="btn btn-sm btn-success mr-2" onClick$={() => handleDataObject(document.getElementById('input-data-object')?.innerHTML as string)}>Update Data Object</button>
              </div>
            </div>
          </div>

        </div>

      
        <div class="card card-compact card-bordered border-b-4 border-zinc-500 bg-stone-50 mt-4 min-h-[400px] w-full">
          <div class="card-body">
            <div class="text-xs font-bold">
              Form Output
            </div>
            {s.dataObjectInput ? (
              <MJsonForm schema={{schema: s.dataSchemaInput}} uischema={{uischema: s.uiSchemaInput}} initialData={{initialData: s.dataObjectInput}} />
            ) : ``}
          </div>
        </div>

      </div>
    </div>
  );
});