import { component$ } from '@builder.io/qwik';

export default component$(() => {
  return (
    <div class="mt-4">
      <div class="text-2xl font-bold">Operator ABC</div>
      <div class="text-md font-bold">in Michigan</div>

      <div class="card card-compact card-bordered mt-4 h-96">
        <div class="card-body">
          <div class="flex justify-between text-xs font-bold">
            <div>Operator Info</div>
            <a>view all operators ❯</a>
          </div>

        </div>

      </div>

      <div class="card card-compact card-bordered mt-4 h-96">
        <div class="card-body">
          <div class="flex justify-between text-xs font-bold">
            <div>Events</div>
          </div>

        </div>

      </div>
      
    </div>
  );
});