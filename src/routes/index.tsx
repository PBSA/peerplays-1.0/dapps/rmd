import { component$, useStore, useVisibleTask$, $} from '@builder.io/qwik';
import type { DocumentHead } from '@builder.io/qwik-city';
import type { ChartItem } from 'chart.js/auto';
import { Chart } from 'chart.js/auto';
import { routeLoader$, server$ } from '@builder.io/qwik-city';
import { MJsonForm, MSelect } from '../integrations/react/react';
import hljs from 'highlight.js/lib/core';
import json from 'highlight.js/lib/languages/json';
import RmdObjMeta from '../components/rmd-obj-meta/rmd-obj-meta';
import CodeOrFormViewer from '../components/code-or-form-viewer/code-or-form-viewer';

hljs.registerLanguage('json', json);

export const useJurisdictions = routeLoader$(async () => {
  let json;
  try {
    const response = await fetch(`http://kuivunja.ddns.net:38005/rmd-api/analysis/get_unique_values/gaming_application/jurisdiction`, {
      method: "GET",
      mode: "cors",
      cache: "no-cache",
      credentials: "same-origin",
      redirect: "follow",
      referrerPolicy: "no-referrer",
    });
    json = await response.json();
    //console.log(json);
    //return JSON.stringify(json);
  }
  catch (e) {
    console.log(e);
    //return [`Error!`];
  }
  return json;
});

const getGamesByJurisdiction = server$(async (selectedJurisdiction: []) => {
  const json: any[] | PromiseLike<any[]> = [];
  //console.log(selectedJurisdiction);

  for (const element of selectedJurisdiction) {
    try {
      // NOTE: Building the URL like this is NOT SAFE! Edit before putting in production!
      const response = await fetch(`http://kuivunja.ddns.net:38005/rmd-api/object/get_many/of_type/gaming_application/require/jurisdiction/${element}`, {
        method: "GET",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        redirect: "follow",
        referrerPolicy: "no-referrer",
      });
      const ret = await response.json();
      

      for (const element of ret.contents) {
        //console.log(element.object.gaming_activity_ref)
        json.push( element );
      }

      //console.log(JSON.stringify(json, null, 2));
      //return JSON.stringify(json);
    }
    catch (e) {
      console.log(e);
      //return [`Error!`];
    }
  }

  /*
  const resArr: any[] = [];
  for (const element of json) {
    const i = resArr.findIndex(x => x.object.gaming_activity_ref == element.object.gaming_activity_ref);
    if(i <= -1){
      resArr.push(element);
    }
  }
  */

  const superObject: any = {}
  const gamingActivityRefIDs: any[] = []
  for (const element of json) {
    if (!Object.prototype.hasOwnProperty.call(superObject, element.object.gaming_activity_ref)) {
      // Key doesn't exist for this gaming_activity_ref
      superObject[element.object.gaming_activity_ref] = [element]
      gamingActivityRefIDs.push({id: element.object.gaming_activity_ref, jurisdiction: element.object.jurisdiction})
    }
    else {
      // Key DOES exist, so lets add the object to the key.
      superObject[element.object.gaming_activity_ref].push(element)
    }
  }

  if (gamingActivityRefIDs.length > 0) {
    superObject['gaming_activity_ref_ids'] = gamingActivityRefIDs

    const gamingActivityDefs: any[] = []
    for (const element of gamingActivityRefIDs) {
      try {
        const response = await fetch(`http://kuivunja.ddns.net:38005/rmd-api/object/get_wrapped/${element.id}`, {
          method: "GET",
          mode: "cors",
          cache: "no-cache",
          credentials: "same-origin",
          redirect: "follow",
          referrerPolicy: "no-referrer",
        });
        const ret = await response.json();
        ret['gaming_activity_ref'] = element.id
        ret['jurisdiction'] = element.jurisdiction
        gamingActivityDefs.push(ret)
        console.log('RET\n', ret);
      }
      catch (e) {
        console.log(e)
      }
    }
    if (gamingActivityDefs.length > 0) {
      superObject['gaming_activity_defs'] = gamingActivityDefs
    }

  }

  //console.log('superObject\n', superObject);
  
  return superObject;
});


const getReportsByGame = server$(async (selectedGame: any[]) => {
  const json: any[] | PromiseLike<any[]> = [];
  //console.log(selectedJurisdiction);

  for (const element of selectedGame) {
    try {
      // NOTE: Building the URL like this is NOT SAFE! Edit before putting in production!
      const response = await fetch(`http://kuivunja.ddns.net:38005/rmd-api/object/get_many/of_type/gaming_activity_report/require/application_ref/${element}`, {
        method: "GET",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        redirect: "follow",
        referrerPolicy: "no-referrer",
      });
      const ret = await response.json();
      //console.log('ret\n', ret);

      for (const element of ret.contents) {
        //console.log(element.object.gaming_activity_ref)
        json.push( element );
      }

      //console.log(JSON.stringify(json, null, 2));
      //return JSON.stringify(json);
    }
    catch (e) {
      console.log(e);
      //return [`Error!`];
    }
  }
  
  return json;
});

export default component$(() => {

  const schema = {
    "type": "object",
    "properties": {
      "entity_ref": {
        "type": "string"
      },
      "activity_description": {
        "type": "string"
      },
      "date_info": {
        "type": "object",
          "properties": {
            "on_date": {"type": "string"},
            "date_begin": {"type": "string"},
            "date_end": {"type": "string"}
          }
      },
      "venue": {
        "type": "string"
      },
      "rating": {
        "type": "integer"
      }
    },
    "required": [
      "entity_ref",
      "activity_description",
      "venue"
    ]
  }

  const uischema = {
    "type": "VerticalLayout",
    "elements": [
      {
        "type": "Control",
        "scope": "#/properties/entity_ref",
        "label": "Entity ID"
      },
      {
        "type": "Control",
        "scope": "#/properties/activity_description",
        "options": {
          "multi": true
        },
        "label": "Describe Proposed Gaming Activity"
      },
      {
        "type": "Control",
        "label": "Venue",
        "scope": "#/properties/venue"
      }
    ]
  }


  const jurisdictionsArray = useJurisdictions().value;

  const s = useStore({
    jurisdiction: "National",
    operator: "",
    event: "",
    searchText: "",
    searchType: "",
    mSelectGameElement: {} as HTMLSelectElement | null,
    superObject: {} as any,
    gameList: [] as any[],
    gameAppsList: [] as any[],
    gameReportsList: [] as any[],
    selectedGameObject: {},
    selectedApplicationId: "",
    selectedApplicationObject: {},
    selectedReportObject: {},
    selectedJurisdictions: 0,
    displayGameObject: false,
    displayApplicationObject: false,
    displayReportObject: false,
    statProceedsToBenefit: 0,
    statProceedsToWinner: 0,
    clearJurisdictions: false,
    clearGames: false,
    isViewingFormGame: true,
    isViewingFormApp: true,
    isViewingFormReport: true,
  })

  useVisibleTask$(({ track }) => {
    track(() => s.selectedReportObject)
    let ggr, goe, pro;
    
    if(Object.keys(s.selectedReportObject).length !== 0){
      ggr = s.selectedReportObject.report_details.ggr_breakdown
      goe = s.selectedReportObject.report_details.gr_reducers
      pro = s.selectedReportObject.report_details.proceeds

      const totalGGR = parseFloat(ggr.online_sales) +  parseFloat(ggr.manual_sales) + parseFloat(ggr.seed_sponsorship);
      const totalGOE = parseFloat(goe.platform_fees) + parseFloat(goe.distributor_fees) + parseFloat(goe.transaction_and_licensing_fees);

      s.statProceedsToBenefit = pro.beneficiaries.reduce((acc: number, cur: { amount: string; }) => acc + parseFloat(cur.amount), 0)
      s.statProceedsToWinner = pro.winners.reduce((acc: number, cur: { amount: string; }) => acc + parseFloat(cur.amount), 0)

      new Chart (document.getElementById('chartGGR') as ChartItem, {
        type: 'pie',
        data: {
          labels: ['Online Sales', 'Manual Sales', 'Seed Sponsorship'],
          datasets: [{
            label: '$',
            data: [
              parseFloat(ggr.online_sales),
              parseFloat(ggr.manual_sales),
              parseFloat(ggr.seed_sponsorship)
            ]
          }]
        },
        options: {
          plugins: {
            title: {
              display: true,
              fullSize: false,
              text: `Gross Gaming Revenue ($${totalGGR} total)`
            }
          }
        }
      })

      new Chart (document.getElementById('chartGOE') as ChartItem, {
        type: 'pie',
        data: {
          labels: ['Platform Fees', 'Distributor Fees', 'Transaction & Licensing Fees'],
          datasets: [{
            label: '$',
            data: [
              parseFloat(goe.platform_fees),
              parseFloat(goe.distributor_fees),
              parseFloat(goe.transaction_and_licensing_fees)
            ]
          }]
        },
        options: {
          plugins: {
            title: {
              display: true,
              fullSize: false,
              text: `Gross Operating Expenses ($${totalGOE} total)`
            }
          }
        }
      })
    }
  })

  const handleGamesByJurisdiction = $(
    async function handleGamesByJurisdiction(jurisdictionList: []) {
      s.superObject = {}
      if(jurisdictionList.length > 0){
        s.superObject = await getGamesByJurisdiction(jurisdictionList)
      }
      else { 
        s.displayGameObject = false
        s.displayApplicationObject = false
        s.displayReportObject = false
        s.gameList = []
        s.gameAppsList = []
        s.gameReportsList = []
      }
    }
  );

  const handleReportsByApplication = $(
    async function handleReportsByApplication(appList: any[]) {
      s.displayReportObject = ""
      if(appList.length > 0){
        const aGameReport = await getReportsByGame(appList)
        //console.log('aGameReport\n', aGameReport)
        s.gameReportsList = aGameReport
      }
      else { s.gameReportsList = [] }
    }
  );

  return (
    <>
        <section>
        <div class="collapse collapse-arrow bg-base-200 mt-4 overflow-visible">
          <input type="checkbox" /> 
          <div class="collapse-title text-xl font-medium">
            Filters
          </div>
          <div class="collapse-content">
            <div class="flex flex-col items-center w-full">
              <div class="w-full">
                <MSelect  client:visible
                          itemType={'jurisdiction'}
                          options={jurisdictionsArray}
                          placeholder={'Select Jurisdiction(s)'}
                          onSelected$={(newVal, actMeta) => {
                            //console.log("newVal =\n", newVal)
                            s.selectedJurisdictions = newVal.length
                            const input = newVal.map((value) => {return(value.value)})
                            handleGamesByJurisdiction(input)
                          }}/>
              </div>

              <div class="w-full mt-2">
                <MSelect  client:visible
                        itemType={'game'}
                        options={[s.superObject]}
                        placeholder={'Select Game'}
                        onSelected$={ async (newVal, actMeta) => {
                          console.log("newVal =\n", newVal.value)
                          s.selectedGameObject = newVal.value
                          s.displayGameObject = true
                          s.selectedApplicationId = newVal.value.gaming_activity_ref
                        }}/>
              </div>

              <div class="w-full mt-2">
                <MSelect  client:visible
                        itemType={'application'}
                        options={s.superObject[s.selectedApplicationId]}
                        placeholder={'Select Application'}
                        onSelected$={ async (newVal, actMeta) => {
                          //console.log("newVal =\n", newVal)
                          s.selectedApplicationObject = newVal.value
                          s.displayApplicationObject = true
                          handleReportsByApplication([newVal.value.object_id])
                        }}/>
              </div>

              <div class="w-full mt-2">
                <MSelect  client:visible
                          itemType={'report'}
                          options={s.gameReportsList}
                          placeholder={'Select Report'}
                          onSelected$={ async (newVal, actMeta) => {
                            //console.log("newVal =\n", newVal)
                            s.selectedReportObject = newVal.value
                            s.displayReportObject = true
                          }}/>
              </div>

            </div>
          </div>
        </div>

        <div class="collapse collapse-arrow bg-base-200 mt-4">
          <input type="checkbox" /> 
          <div class="collapse-title text-xl font-medium">
            Dashboard Charts
          </div>
          <div class="collapse-content">
            

            {Object.keys(s.selectedReportObject).length === 0 ? (
              <div class="font-bold text-sm mt-2">Select a Game Report to View the Dashboard</div>
            ) : (
            <>
            <div class="font-bold text-sm mt-2">Covers 1 event(s) in {s.selectedJurisdictions} jurisdiction(s):</div>
            <div class="flex flex-col">
              <div class="flex flex-col gap-5 items-center">

                <div class="flex">
                  <div class="card card-compact card-bordered border-b-4 border-green-500 bg-green-50 mt-2 w-full">
                    <div class="card-body min-h-[100px]">
                      <div class="flex flex-col items-center my-auto text-xs font-bold ">
                        <canvas id="chartGGR" />
                  </div></div></div>

                  <div class="card card-compact card-bordered border-b-4 border-red-500 bg-red-50 w-full">
                    <div class="card-body min-h-[100px]">
                      <div class="flex flex-col items-center my-auto text-xs font-bold">
                        <canvas id="chartGOE" />
                  </div></div></div>
                </div>



                <div class="card card-compact card-bordered border-b-4 border-orange-500 bg-orange-50 w-full">
                  <div class="card-body min-h-[100px]">
                    <div class="flex flex-col items-center my-auto text-xs font-bold">
                      <div class="text-center">Proceeds to Beneficiaries</div>
                      <div class="text-xl mt-2">${s.statProceedsToBenefit}</div>
                    </div>
                  </div>
                </div>

                <div class="card card-compact card-bordered border-b-4 border-blue-500 bg-blue-50 w-full">
                  <div class="card-body min-h-[100px]">
                    <div class="flex flex-col items-center my-auto text-xs font-bold">
                      <div class="text-center">Proceeds to Winners</div>
                      <div class="text-xl mt-2">${s.statProceedsToWinner}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </>
            )}
          </div>
        </div>

        {s.displayGameObject ? (
          <>
            <CodeOrFormViewer
              rmd_object={(s.selectedGameObject)}
              data_schema={schema}
              ui_schema={uischema}
            />
          </>
        ):(``)}

        {s.displayApplicationObject ? (
          <>
            <CodeOrFormViewer
              rmd_object={(s.selectedApplicationObject)}
              data_schema={undefined}
              ui_schema={undefined}
            />
          </>
        ):(``)}

        {s.displayReportObject ? (
          <>
            <CodeOrFormViewer
              rmd_object={(s.selectedReportObject)}
              data_schema={undefined}
              ui_schema={undefined}
            />
          </>
        ):(``)}

      </section>
    </>
  );
});


export const head: DocumentHead = {
  title: 'Regulator Dashboard'
};
