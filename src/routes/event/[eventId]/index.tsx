import { component$ } from '@builder.io/qwik';

export default component$(() => {
  return (
    <div class="mt-4">
      <div class="text-2xl font-bold">Event XYZ</div>
      <div class="text-md font-bold">by Operator ABC in Michigan</div>

      <div class="card card-compact card-bordered mt-4 h-96">
        <div class="card-body">
          <div class="flex justify-start text-xs font-bold">
            <div>Operator Info</div>
          </div>

        </div>

      </div>

      <div class="flex mt-4 gap-4">
        <div class="card card-compact card-bordered h-96 w-full">
          <div class="card-body">
            <div class="flex justify-start text-xs font-bold">
              <div>Disbursements</div>
            </div>

          </div>

        </div>

        <div class="card card-compact card-bordered h-96 w-full">
          <div class="card-body">
            <div class="flex justify-start text-xs font-bold">
              <div>Totals</div>
            </div>

          </div>

        </div>
      </div>

      <div class="card card-compact card-bordered mt-4 h-96">
        <div class="card-body">
          <div class="flex justify-start text-xs font-bold">
            <div>Event Info</div>
          </div>

        </div>

      </div>
      
    </div>
  );
});