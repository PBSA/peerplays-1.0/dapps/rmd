import { component$, Slot } from '@builder.io/qwik';
import { useLocation } from '@builder.io/qwik-city';
import Header from '../components/header/header';
import Footer from '../components/footer/footer';

export default component$(() => {
  const loc = useLocation();
  const defaultSearchCategory = loc.url.searchParams.get("search") == "allevents" ? "events" : ""
  return (
    <>
      <main>
        <Header defaultSearchCategory={defaultSearchCategory} />

        <section>
          <Slot />
        </section>

      </main>

      <Footer />

    </>
  );
});
