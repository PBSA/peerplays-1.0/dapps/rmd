import { component$, useStore } from '@builder.io/qwik';

export default component$(() => {

  // Variables to play with...
  const myArray = [
    {
      myNumber: 123,
      myName: "Sean"
    },
    {
      myNumber: 234,
      myName: "Chris"
    },
    {
      myNumber: 345,
      myName: "Alice"
    },
    {
      myNumber: 456,
      myName: "Bob"
    }
  ]
  const isRed = true

  // useStore lets the app remember inputs.
  const s = useStore({
    magicNumber: 0
  });

  /** 
   * Note:
   * We're returning HTML with CSS for styling here.
   * This is all interpreted as text that will be rendered
   * as HTML code. To tell TypeScript (TS) where something should
   * be interpreted as JS code, use curly brackets:
   * 
   *         { This is now JS code. }
   * 
   * You can use backtick (`) symbols around text to make
   * a literal string. like this:
   * 
   *    `This is a literal string and doesn't need escaping.`
   * 
   * Notice the apostrophe? It doesn't need escaping because
   * this string is now literal. You can insert code into a
   * literal string too. This time use ${}, like this:
   * 
   *     `This is a literal string ${ This is now JS code }.`
   * 
   * Now you know how to put code throughout the HTML code
   * you'll need to return.
   * */
  return (
    <div class="mt-4">
      {/** 
       * The following line has a ternary operator.
       * This is how to do If-Then statements in TS templates.
       * Note how I had to turn the class property into an object,
       * then I used a string literal with my code put into it.
       * Works like a charm!
       */}
      <div class="flex justify-between">
        <div class={`text-2xl font-bold w-1/2 ${isRed ? `text-red-700` : ``}`}>Numbers & Names</div>
        
        <input class="input input-sm input-bordered w-1/2" 
               type={`number`} 
               placeholder={`search number`} 
               onInput$={(event) => {s.magicNumber = parseInt((event.target as HTMLInputElement).value)}} />

      </div>

      {/**
       * Now I'm going to loop with an array. I can't
       * mutate the array I already have, so I'll use
       * the map function which will copy my array
       * and return a new array plus some transformations
       * that I specify in the map.
       * 
       * The point is to return an array which will
       * contain all the HTML code in it that I want to
       * display here.
       */}

      { myArray.map((element, index) => {

        const highlight = element.myNumber == s.magicNumber

        /** 
         * "element" here is the myArray element we're currently working with.
         * We're now iterating through each element, transforming it, and
         * returning that transformation as a new element in a new array.
         * Also note this structure:
         * <>
         *   <div></div>
         *   <div></div>
         * </>
         * This is because we can only return a single top-level HTML tag
         * that everything must fit in. The simple "<>" and "</>" tags
         * can hold everything and will even pass CSS selectors through.
         */
        
        return(
          <>
          <div class="mt-2">Name #{index + 1}</div>
          <div class={`card card-compact card-bordered ${highlight ? `bg-amber-200` : ``}`}>
            <div class="card-body">
              <div class="flex justify-between text-xl font-bold">
                <div>Name: {element.myName}</div>
                <div>Number: {element.myNumber}</div>
              </div>
            </div>
          </div>
          </>
        )
      }) }
      
    </div>
  );
});