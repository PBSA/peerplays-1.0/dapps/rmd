import { component$, Slot } from '@builder.io/qwik';
import { useLocation } from '@builder.io/qwik-city';
import Header from '../components/header/header';
import Footer from '../components/footer/footer';

export default component$(() => {
  const loc = useLocation();
  const defaultSearchCategory = loc.url.searchParams.get("search") == "allevents" ? "events" : ""
  return (
    <>
      <main>
        <Header defaultSearchCategory={defaultSearchCategory} />

        <section>
          <div class="flex flex-col items-center w-full mt-2">
            <div class="flex items-center justify-between w-full mt-4">
              <div class="mr-2 w-1/4 text-end text-sm">Jurisdiction</div>
              <div class="w-3/4">
                <select class="select select-bordered select-xs min-[321px]:select-sm w-full">
                  <option selected>National</option>
                  <option>Michigan</option>
                </select>
              </div>
            </div>

            <div class="flex items-center justify-between w-full mt-2">
              <div class="mr-2 w-1/4 text-end text-sm">Sort</div>
              <div class="w-3/4">
                <select class="select select-bordered select-xs min-[321px]:select-sm w-full">
                  <option selected>A to Z</option>
                  <option>Z to A</option>
                </select>
              </div>
            </div>

            <div class="flex items-center justify-between w-full mt-2">
              <div class="mr-2 w-1/4 text-end text-sm">Filter</div>
              <div class="w-3/4">
                <select class="select select-bordered select-xs min-[321px]:select-sm w-full">
                  <option selected>Filters</option>
                  <option>Date Range</option>
                </select>
              </div>
            </div>
          </div>
          <Slot />
        </section>

      </main>

      <Footer />

    </>
  );
});
